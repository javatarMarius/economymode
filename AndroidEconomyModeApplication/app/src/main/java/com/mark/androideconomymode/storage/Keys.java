package com.mark.androideconomymode.storage;

/**
 * Created by mark on 12.08.2015.
 */
public final class Keys {

    public static final String START_TIME = "START_TIME";
    public static final String FINISH_TIME = "FINISH_TIME";

    public static final String GPS_STATUS = "GPS_STATUS";
    public static final String WIFI_STATUS = "WIFI_STATUS";
    public static final String BLUETOOTH_STATUS = "BLUETOOTH_STATUS";
}
