package com.mark.androideconomymode.storage;

import android.content.Context;
import android.content.SharedPreferences;

public class SaveValues {

    private static SaveValues INSTANCE;
    private final String STORAGE_NAME = "com.mark.androideconomymode";

    private SaveValues() {
    }

    public static synchronized SaveValues getInstance() {
        if (SaveValues.INSTANCE == null) {
            INSTANCE = new SaveValues();
        }
        return INSTANCE;
    }

    public void putValue(final Context context, final String key, final String value) {
        getEditor(context).putString(key, value).commit();
    }

    public void putValue(final Context context, final String key, final long value) {
        getEditor(context).putLong(key, value).commit();
    }

    public void putValue(final Context context, final String key, final Integer value) {
        getEditor(context).putInt(key, value).commit();
    }

    public void putValue(final Context context, final String key, final boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public String getStrValue(final Context context, final String key) {
        return getSettings(context).getString(key, "");
    }

    public int getIntValue(final Context context, final String key) {
        return getSettings(context).getInt(key, 0);
    }

    public long getLongValue(final Context context, final String key) {
        return getSettings(context).getLong(key, 0);
    }

    public boolean getBoolValue(final Context context, final String key) {
        return getSettings(context).getBoolean(key, false);
    }

    public void clearAllData(final Context context) {
        getEditor(context).clear().commit();
    }

    public void removeDataByKey(final Context context, final String key) {
        getEditor(context).remove(key).commit();
    }

    private SharedPreferences.Editor getEditor(final Context context) {
        return getSettings(context).edit();
    }

    private SharedPreferences getSettings(final Context context) {
        return context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
    }
}
