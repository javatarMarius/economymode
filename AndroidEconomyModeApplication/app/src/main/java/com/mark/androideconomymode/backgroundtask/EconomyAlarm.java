package com.mark.androideconomymode.backgroundtask;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import com.mark.androideconomymode.R;
import com.mark.androideconomymode.storage.Keys;
import com.mark.androideconomymode.storage.SaveValues;
import com.mark.androideconomymode.utils.Utils;

import java.util.Calendar;

public class EconomyAlarm extends BroadcastReceiver {

    public static boolean isStart = false;
    public static boolean isFinish = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        checkStatus(context);

    }

    private void checkStatus(final Context context) {
        final String currTime = getCurrentTime(context);
        final String startTime = SaveValues.getInstance().getStrValue(context, Keys.START_TIME);
        final String finishTime = SaveValues.getInstance().getStrValue(context, Keys.FINISH_TIME);
        if (currTime.equals(startTime) && !isStart) {
            isStart = true;
            Utils.scanAndSaveWifiGpsBluetoothStatus(context);
            Utils.disableWifiGpsBluetooth(context);
            sendNotification(context, "У Вас будут отключены wifi, gps, bluetooth", "Внимание");
        }
        if (currTime.equals(finishTime) && !isFinish) {
            sendNotification(context, "У Вас будут включены wifi, gps, bluetooth", "Внимание");
            isFinish = true;
            Utils.restoreWifiGpsBlietoothStatus(context);
            Intent stopIntent = new Intent(context, MyService.class);
            context.stopService(stopIntent);
        }
    }

    private String getCurrentTime(final Context context) {
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getTimeFormat(context);
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public void startAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, EconomyAlarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 10000, pi);
    }

    public void cancelAlarm(Context context) {
        Intent intent = new Intent(context, EconomyAlarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    private NotificationManager mNotificationManager;
    public static final int NOTIFICATION_ID = 1;

    private void sendNotification(final Context context, final String msg, final String title) {
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setAutoCancel(true)
                .setContentText(msg)
                .setSmallIcon(R.drawable.ic_launcher)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
        long[] pattern = {300, 300, 300, 300, 300};
        mBuilder.setVibrate(pattern);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
