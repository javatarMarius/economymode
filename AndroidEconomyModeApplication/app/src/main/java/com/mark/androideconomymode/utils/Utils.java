package com.mark.androideconomymode.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.provider.Settings;

import com.mark.androideconomymode.factory.ToastFactory;
import com.mark.androideconomymode.storage.Keys;
import com.mark.androideconomymode.storage.SaveValues;

/**
 * Created by mark on 12.08.2015.
 */
public class Utils {

    private static final String GPS = "gps";
    private static final String PACKAGE_NAME = "com.android.settings";
    private static final String CLASS_NAME = "com.android.settings.widget.SettingsAppWidgetProvider";

    public static void scanAndSaveWifiGpsBluetoothStatus(final Context context) {
        wifiStatus(context);
        bluetoothStatus(context);
        gpsStatus(context);
    }

    public static void setWifiStatus(final Context context, boolean flag) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(flag);
    }

    public static void setBluetoothStatus(boolean flag) {
        BluetoothAdapter bluetooth = getBluetoothAdapter();
        if (bluetooth == null) {
            // Device does not support Bluetooth
            return;
        }
        if (flag) {
            bluetooth.enable();
        } else {
            bluetooth.disable();
        }
    }

    public static void setGpsStatus(final Context context, boolean flag) {
        if (flag) {
            turnGpsOn(context);
        } else {
            turnGpsOff(context);
        }
    }

    public static void disableWifiGpsBluetooth(final Context context) {
        Utils.setWifiStatus(context, false);
        Utils.setBluetoothStatus(false);
        Utils.setGpsStatus(context, false);
    }

    public static void restoreWifiGpsBlietoothStatus(final Context context) {
        SaveValues sv = SaveValues.getInstance();
        boolean wifiFlag = sv.getBoolValue(context, Keys.WIFI_STATUS);
        boolean blFlag = sv.getBoolValue(context, Keys.BLUETOOTH_STATUS);
        boolean gpsFlag = sv.getBoolValue(context, Keys.GPS_STATUS);
        Utils.setWifiStatus(context, wifiFlag);
        Utils.setBluetoothStatus(blFlag);
        Utils.setGpsStatus(context, gpsFlag);
    }

    private static void turnGpsOn(final Context context) {
        String provider = getProvider(context);
        if (!provider.contains(GPS)) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName(PACKAGE_NAME, CLASS_NAME);
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            context.sendBroadcast(poke);
        }
    }

    private static void turnGpsOff(final Context c) {
        String provider = getProvider(c);
        if (provider.contains(GPS)) { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName(PACKAGE_NAME, CLASS_NAME);
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            c.sendBroadcast(poke);
        }
    }

    private static String getProvider(final Context c) {
        return Settings.Secure.getString(c.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
    }

    private static BluetoothAdapter getBluetoothAdapter() {
        return BluetoothAdapter.getDefaultAdapter();
    }

    private static void wifiStatus(final Context c) {
        final ConnectivityManager connManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        SaveValues.getInstance().putValue(c, Keys.WIFI_STATUS, wifi.isConnected());
    }

    private static void bluetoothStatus(final Context c) {
        BluetoothAdapter bluetooth = getBluetoothAdapter();
        if (bluetooth == null) {
            // Device does not support Bluetooth
            ToastFactory.showToast(c, "Device does not support Bluetooth");
            return;
        }
        SaveValues.getInstance().putValue(c, Keys.BLUETOOTH_STATUS, bluetooth.isEnabled());
    }

    private static void gpsStatus(final Context c) {
        final LocationManager locManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        boolean flag = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        SaveValues.getInstance().putValue(c, Keys.GPS_STATUS, flag);
    }
}
