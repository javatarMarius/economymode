package com.mark.androideconomymode.backgroundtask;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.mark.androideconomymode.factory.ToastFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyService extends Service {

    private EconomyAlarm mEconomyAlarm;
    private ExecutorService mExecutorService;

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initNewThread();
        mEconomyAlarm = null;
        mEconomyAlarm = new EconomyAlarm();
        check();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        mEconomyAlarm.cancelAlarm(this);
        mExecutorService.shutdownNow();
        ToastFactory.showToast(this, "ALL DISABLED");
        EconomyAlarm.isStart = false;
        EconomyAlarm.isFinish = false;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initNewThread() {
        if (mExecutorService != null) {
            mExecutorService.shutdownNow();
            mExecutorService = null;
        }
        mExecutorService = Executors.newFixedThreadPool(1);
    }

    private void check() {
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                mEconomyAlarm.startAlarm(MyService.this);
            }
        });
    }
}
