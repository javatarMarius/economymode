package com.mark.androideconomymode;

import android.app.Activity;
import android.os.Bundle;

public class EconomyActivity extends Activity {

    private MainAppView mMainAppView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_economy);
        show();
    }

    @Override
    protected void onDestroy() {
        mMainAppView.destroy();
        mMainAppView = null;
        super.onDestroy();
    }

    private void show() {
        mMainAppView = new MainAppView(this);
        mMainAppView.initElements();
    }
}
