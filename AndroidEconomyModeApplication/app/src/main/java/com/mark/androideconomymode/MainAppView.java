package com.mark.androideconomymode;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.mark.androideconomymode.backgroundtask.MyService;
import com.mark.androideconomymode.factory.ToastFactory;
import com.mark.androideconomymode.storage.Keys;
import com.mark.androideconomymode.storage.SaveValues;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by mark on 12.08.2015.
 */
public class MainAppView {

    private long mStart;
    private long mFinish;

    private Switch mBtnOnOff;
    private Activity mActivity;
    private Calendar mCalendarEnd;
    private Calendar mCalendarStart;
    private TimePicker mTimePickerFinish;
    private TextView mYourTimeInterval;
    private TimePicker mTimePickerStart;

    private String mStartTime;
    private String mFinishTime;

    private boolean mIsSetStartTime = false;
    private boolean mIsSetFinishTime = false;

    public MainAppView(Activity activity) {
        mActivity = activity;
    }

    public void initElements() {
        initTimePickers();
        initCalendars();
//        initIntervals();
        initSwitchButton();
        mYourTimeInterval = (TextView) mActivity.findViewById(R.id.your_time_interval);
    }

    private void initTimePickers() {
        mTimePickerStart = getTimePicker(R.id.on_timepicker);
        mTimePickerFinish = getTimePicker(R.id.off_timepicker);
        timePickersSettings();
    }

    private void initCalendars() {
        mCalendarStart = Calendar.getInstance();
        mCalendarEnd = Calendar.getInstance();
    }

    private void initSwitchButton() {
        mBtnOnOff = (Switch) mActivity.findViewById(R.id.on_off_button);
        switchBtnSettings();
    }

    private void switchBtnSettings() {
        setCheckedChange();
        setClick();
    }

    private void setCheckedChange() {
        mBtnOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mIsSetStartTime && mIsSetFinishTime) {
                    if (mBtnOnOff.isChecked()) {
                        startEconomyService();
                    } else {
                        stopEconomyService();
                    }
                } else {
                    showErrorMessage();
                }
            }
        });
    }

    private void setClick() {
        mBtnOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsSetStartTime && mIsSetFinishTime) {
                    if (mBtnOnOff.isChecked()) {
                        startEconomyService();
                    } else {
                        stopEconomyService();
                    }
                } else {
                    showErrorMessage();
                }
            }
        });
    }

    private void startEconomyService() {
        enabledOrDisabledTimePicker(false);
        calculateEconomyInterval();
        Intent intent = new Intent(mActivity, MyService.class);
        mActivity.startService(intent);
    }

    private void stopEconomyService() {
        enabledOrDisabledTimePicker(true);
        mActivity.stopService(new Intent(mActivity, MyService.class));
    }

    private void enabledOrDisabledTimePicker(boolean flag) {
        mTimePickerStart.setEnabled(flag);
        mTimePickerFinish.setEnabled(flag);
        mTimePickerStart.setFocusable(flag);
        mTimePickerFinish.setFocusable(flag);
    }

    private void showErrorMessage() {
        ToastFactory.showToast(mActivity, "Установите интервал работы приложения");
        mBtnOnOff.setChecked(false);
    }

    private String getDateFormat(Date date) {
        return android.text.format.DateFormat.getTimeFormat(mActivity).format(date);
    }

    private void showYourInterval() {
        mYourTimeInterval.setText("Начало: " + mStartTime + " Конец: " + mFinishTime);
    }

    private void timePickersSettings() {
        Calendar now = Calendar.getInstance();
        mTimePickerStart.setIs24HourView(true);
        mTimePickerFinish.setIs24HourView(true);
        mTimePickerStart.setCurrentHour(now.get(Calendar.HOUR_OF_DAY));
        mTimePickerFinish.setCurrentHour(now.get(Calendar.HOUR_OF_DAY));
        mTimePickerStart.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
        mTimePickerFinish.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
        setTimePickersListeners();
    }

    private void setTimePickersListeners() {
        mTimePickerStart.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                mIsSetStartTime = true;
                calculateStartTime();
            }
        });

        mTimePickerFinish.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                mIsSetFinishTime = true;
                calculateFinishTime();
            }
        });
    }

    private void calculateStartTime() {
        final Calendar calendar = Calendar.getInstance();
        final int hour = mTimePickerStart.getCurrentHour();
        final int minute = mTimePickerStart.getCurrentMinute();
        final int year = mCalendarStart.get(Calendar.YEAR);
        final int month = mCalendarStart.get(Calendar.MONTH);
        final int day = mCalendarStart.get(Calendar.DAY_OF_MONTH);
        mCalendarStart.set(year, month, day, hour, minute);
        // проверка на корректность ввода интервала
        if (mCalendarStart.getTime().getTime() < calendar.getTime().getTime()) {
            mIsSetStartTime = false;
            return;
        }
        final Date date = mCalendarStart.getTime();
        mStart = date.getTime();
        mStartTime = getDateFormat(date);
        SaveValues.getInstance().putValue(mActivity, Keys.START_TIME, mStartTime);
    }

    private void calculateFinishTime() {
        final Calendar calendar = Calendar.getInstance();
        final int hour = mTimePickerFinish.getCurrentHour();
        final int minute = mTimePickerFinish.getCurrentMinute();
        final int year = mCalendarEnd.get(Calendar.YEAR);
        final int month = mCalendarEnd.get(Calendar.MONTH);
        final int day = mCalendarEnd.get(Calendar.DAY_OF_MONTH);
        mCalendarEnd.set(year, month, day, hour, minute);
        // проверка на корректность ввода интервала
        if (mCalendarEnd.getTime().getTime() < calendar.getTime().getTime()) {
            mIsSetFinishTime = false;
            return;
        }
        final Date date = mCalendarEnd.getTime();
        mFinish = date.getTime();
        mFinishTime = getDateFormat(date);
        SaveValues.getInstance().putValue(mActivity, Keys.FINISH_TIME, mFinishTime);
    }

    private void calculateEconomyInterval() {
        if (mStart < mFinish) {
            showYourInterval();
        } else {
            String tmpValue = mFinishTime;
            mFinishTime = mStartTime;
            mStartTime = tmpValue;
            showYourInterval();
            SaveValues.getInstance().putValue(mActivity, Keys.START_TIME, mStartTime);
            SaveValues.getInstance().putValue(mActivity, Keys.FINISH_TIME, mFinishTime);
        }
    }

    private TimePicker getTimePicker(final int id) {
        return (TimePicker) mActivity.findViewById(id);
    }

    public void destroy() {
        mActivity = null;
        mTimePickerStart = null;
        mTimePickerFinish = null;
        mBtnOnOff = null;
        mYourTimeInterval = null;
    }
}
