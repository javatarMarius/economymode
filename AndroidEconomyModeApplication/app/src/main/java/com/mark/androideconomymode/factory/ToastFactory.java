package com.mark.androideconomymode.factory;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class ToastFactory {

    public static void showToast(final Context context, final String message) {
        if (context != null) {
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public static void showToast(final Context context, int message) {
        if (context != null) {
            Toast toast = Toast.makeText(context, context.getString(message), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
}
